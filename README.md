# [Provee] Dummy Projector Microservice

This microservice (dummy) will serve a grcp endpoint and publish every x milliseconds a new random projection point

The front-end application consists of a server and a client written in [React](https://reactjs.org/).

## Prerequisites for running the service

Clone the quickstarts repository

```bash
git clone [-b <dapr_version_tag>] https://git.science.uu.nl/vig/provee/dummy-projector.git
```

> **Note**: See https://github.com/dapr/quickstarts#supported-dapr-runtime-version for supported tags.

### Run locally

Install [Python3](https://www.python.org/downloads/)

### Run in Kubernetes environment

1. Dapr-enabled Kubernetes cluster. Follow [these instructions](https://github.com/dapr/docs/blob/master/getting-started/environment-setup.md#installing-dapr-on-a-kubernetes-cluster) to set this up.

## Starting the service locally

Open a terminal window and navigate to the project root directory and follow the steps below:

- Install required packages
  ```
  pip3 install -r requirements.txt
  ```
- Set environment variable to use non-default app port 5000

  ```
  Linux/Mac OS:
  export FLASK_RUN_PORT=5000

  Windows:
  set FLASK_RUN_PORT=5000
  ```

- Start dapr using the command:

  ```
  dapr run --app-id dummyprojector --app-port 5000 --port 3501 flask run
  ```

7. Open your browser's console window (using F12 key) to see the logs produced as we use the calculator. Note that each time we click a button, we see logs that indicate state persistence and the different apps that are contacted to perform the operation.

## Running the quickstart in a Kubernetes environment

1. Navigate to the deploy directory in this quickstart directory: `cd deploy`
2. Follow [these instructions](https://github.com/dapr/docs/blob/master/howto/setup-state-store/setup-redis.md) to create and configure a Redis store
3. Deploy all of your resources: `kubectl apply -f .`.
   > **Note**: Services could also be deployed one-by-one by specifying the .yaml file: `kubectl apply -f go-adder.yaml`.

Each of the services will spin up a pod with two containers: one for your service and one for the Dapr sidecar. It will also configure a service for each sidecar and an external IP for our front-end, which allows us to connect to it externally.

4. Wait until your pods are in a running state: `kubectl get pods -w`

```bash

NAME                                    READY     STATUS    RESTARTS   AGE
dapr-operator-775c97497c-p92mf          1/1       Running   0          134m
dapr-placement-58c7d5f9cf-l9wcv         1/1       Running   0          134m
dapr-sidecar-injector-5879986bdc-nwdps  1/1       Running   0          134m
dummyprojector-6d85b88cb4-vh7nz         2/2       Running   0          1m
```

5. Next, let's take a look at our services and wait until we have an external IP configured for our front-end: `kubectl get svc -w`

   ```bash
   NAME                          TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)            AGE
   dapr-api                      ClusterIP      10.103.71.22   <none>          80/TCP             135m
   dapr-placement                ClusterIP      10.103.53.127  <none>          80/TCP             135m
   dapr-sidecar-injector         ClusterIP      10.104.220.35  <none>          443/TCP            135m
   dummyprojector-dapr           ClusterIP      10.0.146.253   <none>          80/TCP,50001/TCP   2m
   ```

   Each service ending in "-dapr" represents your services respective sidecars, while the `provee-front-end` service represents the external load balancer for the React/Angular front-end.

   > **Note:** Minikube users cannot see the external IP. Instead, you can use `minikube service [service_name]` to access loadbalancer without external IP.

6. Take the external IP address for `provee-front-end` and drop it in your browser and voilà! You have a working provee system!

   **For Minikube users**, execute the below command to open calculator on your browser

   ```
   $ minikube service provee-front-end
   ```

![Provee Screenshot](./img/provee-screenshot.JPG)

Our client code calls to an Express server, which routes our calls through Dapr to our back-end services. In this case we're calling the divide endpoint on our nodejs application.

## Cleanup

### Local setup cleanup

- Stop all running applications
  ```
  dapr stop --app-id {application id}
  ```
- Uninstall node modules by navigating to the node directory and run:
  ```
  npm uninstall
  ```

### Kubernetes environment cleanup

- Once you're done, you can spin down your Kubernetes resources by navigating to the `./deploy` directory and running:

  ```bash
  kubectl delete -f .
  ```

This will spin down each resource defined by the .yaml files in the `deploy` directory, including the state component.

## The Role of Dapr

Dapr as a programming model for simplifying the development of distributed systems. It is enabling polyglot programming, service discovery and simplified state management.

### Polyglot Programming

Each service can be written in a different programming language, but they're used together in the same larger application. Dapr itself is language agnostic - none of our services have to include any dependency in order to work with Dapr. This empowers developers to build each service however they want, using the best language for the job or for a particular dev team.

### Service Invocation

When our front-end server calls the respective operation services (see `server.js` code below), it doesn't need to know what IP address they live at or how they were built. Instead it calls their local dapr side-car by name, which knows how to invoke the method on the service, taking advantage of the platform’s service discovery mechanism, in this case Kubernetes DNS resolution.

The code below shows calls to the “add” and “subtract” services via the Dapr URLs:

```js
const daprUrl = `http://localhost:${daprPort}/v1.0/invoke`;

app.post('/calculate/add', async (req, res) => {
  const addUrl = `${daprUrl}/addapp/method/add`;
  req.pipe(request(addUrl)).pipe(res);
});

app.post('/calculate/subtract', async (req, res) => {
  const subtractUrl = `${daprUrl}/subtractapp/method/subtract`;
  req.pipe(request(subtractUrl)).pipe(res);
});
...
```

Microservice applications are dynamic with scaling, updates and failures causing services to change their network endpoints. Dapr enables you to call service endpoints with a consistent URL syntax, utilizing the hosting platform’s service discovery capabilities to resolve the endpoint location.

### Simplified State Management

Dapr sidecars provide state management. We can persist our state whenever needed. This means we can refresh the page, close the page or even take down our `provee-front-end` pod, and still retain the same state when we next open it. Dapr adds a layer of indirection so that our app doesn't need to know where it's persisting state. It doesn't have to keep track of keys, handle retry logic or worry about state provider specific configuration. All it has to do is GET or POST against its Dapr sidecar's state endpoint: `http://localhost:3500/v1.0/state/${stateStoreName}`.

Take a look at `server.js` in the Dapr quickstart `react-calculator` directory. Note that it exposes two state endpoints for the React client here to get and set state: the GET `/state` endpoint and the POST `/persist` endpoint. Both forward client calls to the Dapr state endpoint:

```js
const stateUrl = `http://localhost:${daprPort}/v1.0/state/${stateStoreName}`;
```

Our client persists state by simply POSTing JSON key-value pairs (see `react-calculator/client/src/component/App.js`):

```js
const state = [
  {
    key: "calculatorState",
    value,
  },
];

fetch("/persist", {
  method: "POST",
  body: JSON.stringify(state),
  headers: {
    "Content-Type": "application/json",
  },
});
```
