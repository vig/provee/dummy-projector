#
"""The Python implementation of the gRPC route guide server."""

from concurrent import futures
import time
import math
import logging

import grpc

import protos_generated.projector_pb2
import protos_generated.projector_pb2_grpc

import use_cases.projection_use_cases as uc

import os


class DummyProjectorServicer(protos_generated.projector_pb2_grpc.ProjectorServicer):
    """Provides methods that implement functionality of route guide server."""

    # def __init__(self):
    # self.db = route_guide_resources.read_route_guide_database()

    def GetProjectionPoints(self, request, context):
        # rpc GetProjectionPoints(TrainingSet) returns (stream Point) {}
        trainingSet = protos_generated.projector_pb2.TrainingSet()

        proj_use_case = None
        if trainingSet:
            proj_use_case = uc.ProjectionUseCaseWithTrainingset(trainingSet)
        else:
            proj_use_case = uc.ProjectionUseCaseWithoutTrainingset()

        for point in proj_use_case.get_iterator():
            print("Sending Point with %s id and x = %s, y = %s" %
                  (point.id, point.x, point.y))
            yield point


def serve():
    # dapr_port = os.getenv("DAPR_GRPC_PORT", 3500)
    # dapr_url = "http://localhost:{}".format(dapr_port)

    old_url = 'localhost:50051'

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    protos_generated.projector_pb2_grpc.add_ProjectorServicer_to_server(
        DummyProjectorServicer(), server)
    server.add_insecure_port(old_url)
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    """
    This main is for debugging only
    within dapr things are handled with the pub-sub mechanism
    """

    logging.basicConfig()
    serve()
