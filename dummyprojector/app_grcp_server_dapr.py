#
"""The Python implementation of the gRPC route guide server."""

import _thread
import random
import sys
import math
from flask_cors import CORS
from flask import request, jsonify
import flask
import os
import socket
from concurrent import futures
import time
import logging

import grpc


import protos_generated.projector_pb2_grpc

import use_cases.projection_use_cases as uc
# import use_cases.projection_use_cases as uc

# import protos_generated
# import use_cases


class DummyProjectorServicer(protos_generated.projector_pb2_grpc.ProjectorServicer):
    """Provides methods that implement functionality of route guide server."""

    # def __init__(self):
    # self.db = route_guide_resources.read_route_guide_database()

    def GetProjectionPoints(self, request, context):
        # rpc GetProjectionPoints(TrainingSet) returns (stream Point) {}
        print("Accessed GetProjectionPoints", flush=True)

        trainingSet = None
        if not request:
            trainingSet = protos_generated.projector_pb2.TrainingSet()
            print(
                f"NOTTTTTTTTTTTT IMPLEMENTED YET get Trainingset from {request}", flush=True)

        print(f"Retrieved Trainingset {request}", flush=True)

        proj_use_case = None
        if trainingSet:
            proj_use_case = uc.ProjectionUseCaseWithTrainingset(
                trainingSet)
        else:
            proj_use_case = uc.ProjectionUseCaseWithoutTrainingset()

        for point in proj_use_case.get_iterator():
            print("Sending Point with %s id and x = %s, y = %s" %
                 (point.id, point.x, point.y))
                  
            yield point



app = flask.Flask(__name__)
CORS(app)

# importing socket module
# getting the hostname by socket.gethostname() method
hostname = socket.gethostname()
# getting the IP address using socket.gethostbyname() method
ip_address = "localhost"# socket.gethostbyname(hostname)


port = random.randint(50000, 50500)
port = 50460 #I was having issues with other ports
server = None

#https://stackoverflow.com/questions/57599354/python-not-able-to-connect-to-grpc-channel-failed-to-connect-to-all-addresse
if os.environ.get('https_proxy'):
 del os.environ['https_proxy']
if os.environ.get('http_proxy'):
 del os.environ['http_proxy']
 

def startgRPCServer(server_url, delay):
    print(f"Starting grpc server now", flush=True)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1000))
    protos_generated.projector_pb2_grpc.add_ProjectorServicer_to_server(
        DummyProjectorServicer(), server)

    server.add_insecure_port(server_url)
    server.start()
    started = server.wait_for_termination()


@app.route('/start', methods=['POST'])
def start_subscriber():
    server_url = ip_address + ":" + str(port)
    print(f"Requested Server Address: {server_url}", flush=True)

    response = {}
    try:
        _thread.start_new_thread(startgRPCServer, (server_url, 1, ))
        started = True

        response = {
            "message": "GRPC server started: {server_url} {started}",
            "successful": started,
            "url": server_url
        }
    except Exception as exc:
        response = {
            "message": "GRPC server could not be started",
            "successful": False,
            "error": exc
        }

    return jsonify(response)


@app.route('/url', methods=['GET'])
def getData_subscriber():
    # printing the hostname and ip_address
    print(f"Hostname: {hostname}")
    print(f"Resolved IP Address: {ip_address}")

    connection = {
        "app-id": "projector",
        "name": "projector",
        "description": "gives back an grcp Point stream defined in protos/v3/projector.proto under the url in the url field",
        "ip_address": ip_address,
        "port": port,
        "hostname": hostname
    }
    return jsonify(connection)


# logging.basicConfig()
app.run()

# run with: `dapr run --app-id projector --app-port 5000 python ./dummyprojector/app_grcp_server_dapr.py`
# notes: for some reason the port is important here (tells dapr that flask is running on 5000)
