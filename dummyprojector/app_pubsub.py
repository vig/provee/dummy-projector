"""The Python implementation of the pubsub dummy projection points server."""

import os
import sys
import json
import dirtyjson
from flask_cors import CORS
from flask import request, jsonify
import flask

from concurrent import futures
import time
import math
import logging

import grpc
import requests

import protos_generated.projector_pb2
import protos_generated.projector_pb2_grpc

import use_cases.projection_use_cases as uc


class DummyProjectorServicer(protos_generated.projector_pb2_grpc.ProjectorServicer):
    """Provides methods that implement functionality of dummy projection points server."""

    name = "dummyprojector"

    # def __init__(self):
    # self.db = route_guide_resources.read_route_guide_database()

    def GetProjectionPoints(self, request=None, context=None):
        # rpc GetProjectionPoints(TrainingSet) returns (stream Point) {}

        trainingSet = None
        if request:
            trainingSet = protos_generated.projector_pb2.TrainingSet(request)
        else:
            trainingSet = protos_generated.projector_pb2.TrainingSet()

        proj_use_case = None
        if trainingSet:
            proj_use_case = uc.ProjectionUseCaseWithTrainingset(trainingSet)
        else:
            proj_use_case = uc.ProjectionUseCaseWithoutTrainingset()

        for point in proj_use_case.get_iterator():
            print("Generated Point with id %s and x = %s, y = %s" %
                  (point.id, point.x, point.y))
            yield point


app = flask.Flask(__name__)
CORS(app)

dapr_port = os.getenv("DAPR_HTTP_PORT", 3500)

projector = DummyProjectorServicer()


@app.route('/dapr/subscribe', methods=['GET'])
def subscribe():
    subscriptions = [
        {'topic': 'test', 'route': 'test', 'pubsubname': 'pubsub'},
        {'topic': 'available', 'route': 'available', 'pubsubname': 'pubsub'},
        {'topic': 'points', 'route': 'points', 'pubsubname': 'pubsub'},
        {'topic': 'list', 'route': 'list', 'pubsubname': 'pubsub'},
        {'topic': 'A', 'route': 'A', 'pubsubname': 'pubsub'},
    ]
    return jsonify(subscriptions)


@ app.route('/test', methods=['POST'])
def test_subscriber():
    print(f'test: {request.json}', flush=True)
    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


@ app.route('/available', methods=['POST'])
def name_subscriber():
    print(f'Name: {request.json}', flush=True)

    publishEvent("list", {"name": projector.name})

    return json.dumps({'available': projector.name}), 200, {'ContentType': 'application/json'}


def publishEvent(eventName, jsonPayload):
    dapr_url = "http://localhost:{0}/v1.0/publish/{1}".format(
        dapr_port, eventName)
    res = requests.post(dapr_url, json={"name": projector.name})
    print(
        f'sending off new events to {dapr_url} for list: {res}', flush=True)


@ app.route('/points', methods=['POST'])
def points_subscriber():
    print(
        f'Points request endpoint: {request.json} {type(request.json)}', flush=True)

    if not "data" in request.json:
        return json.dumps({'success': False}), 500, {'ContentType': 'application/json'}

    dataObj = request.json["data"]
    # print(f'data dict {dataObj} {type(dataObj)}')

    dataDict = None
    if isinstance(dataObj, str):
        dataDict = json.loads(dataObj)
    else:
        dataDict = dataObj

    # print(f'dataDict {json.dumps(dataDict, indent = 4)} {type(dataDict)}')

    projName = None

    try:
        if "message" in dataDict:
            # happens when send from react-form client
            msg = dataDict["message"]

            # print(f'message: {msg} {type(msg)}', flush=True)
            if isinstance(msg, str):
                msg = msg.replace("'", '"')
                if not msg.startswith("'") and not msg.endswith("'"):
                    msg = "'{}'".format(msg)

                print(
                    f'message after processing: {msg} {type(msg)}', flush=True)
                try:
                    msgDict = json.loads(msg)
                    print(
                        f'message dict after processing {json.dumps(msgDict, indent = 4)} {type(msgDict)}', flush=True)
                    projName = msgDict["projector"]
                except ValueError as e:
                    print(f'dirtyjson error: {e}', flush=True)
            else:
                projName = msg["projector"]
        else:
            # happens when send from eg. cli
            projName = dataDict["projector"]
    except ValueError as error:  # includes simplejson.decoder.JSONDecodeError
        print(f'Decoding JSON has failed: {error}', flush=True)

    # projname = dataDict["projector"]
    print(f'projname {projName}', flush=True)

    # if projName is not None:
    #     print(f'requested projector {projName}', flush=True)
    #     return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    # else:
    #     print(f'no projector name in {request.json}', flush=True)
    #     return json.dumps({'success': False}), 500, {'ContentType': 'application/json'}

    # # dict = request.json
    # # print(f'json dict {json.dumps(dict, indent = 4)}')
    # # jsonDict = json.loads(request.get_json())
    # # jsonDict = request.json["data"]["message"]

    # # if not jsonDict or not "projector" in jsonDict:
    # #     print(f'no projector name in {jsonDict}', flush=True)
    # #     return json.dumps({'success': False}), 500, {'ContentType': 'application/json'}

    # if projname == projector.name:
    for point in projector.GetProjectionPoints():
        publishEvent("pointavailable", point)
        print(f'publish new point == pointavailable: {point}', flush=True)
        # time.sleep(1)

    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


def _finditem(obj, key):
    if key in obj:
        return obj[key]
    for k, v in obj.items():
        if isinstance(v, dict):
            item = _finditem(v, key)
            if item is not None:
                return item


@ app.route('/list', methods=['POST'])
def list_subscriber():
    print(
        f'listing all currently available projectors: {request.json}', flush=True)
    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


@ app.route('/A', methods=['POST'])
def a_subscriber():
    print(f'A: {request.json}', flush=True)
    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


app.run()

# http://localhost:56373/v1.0/publish/list
# http://localhost:56313/v1.0/publish/list/

# def serve():
#     dapr_port = os.getenv("DAPR_GRPC_PORT", 3500)
#     dapr_url = "http://localhost:{}".format(dapr_port)

#     # old_url = 'localhost:50051'

#     server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
#     protos_generated.projector_pb2_grpc.add_ProjectorServicer_to_server(
#         DummyProjectorServicer(), server)
#     server.add_insecure_port(dapr_url)
#     server.start()
#     server.wait_for_termination()


# if __name__ == '__main__':
#     """
#     This main is for debugging only
#     within dapr things are handled with the pub-sub mechanism
#     """

#     logging.basicConfig()
#     serve()
