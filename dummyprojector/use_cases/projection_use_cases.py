from abc import ABC, ABCMeta, abstractmethod

#from dummyprojector.protos_generated import projector_pb2 as pb

import protos_generated.projector_pb2 as pb

import random


class ProjectionUseCase(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self._isStarted = True
        self._numPoints = 1

    @abstractmethod
    def __iter__(self):
        while False:
            yield None

    def get_iterator(self):
        return self.__iter__()


class ProjectionUseCaseWithTrainingset(ProjectionUseCase):
    def __init__(self, *args):
        self._trainingset = args[0]
        super(ProjectionUseCaseWithTrainingset, self).__init__()

    def __iter__(self):
        print(f"iterator with trainingset {self._isStarted} {self._numPoints} {self._trainingset.rows}", flush=True)
        while self._isStarted and self._numPoints < len(self._trainingset.rows) + 1:
            p = pb.Point(id=self._numPoints, x=random.randint(
                0, 100), y=random.randint(0, 100))
            self._numPoints = self._numPoints + 1
            yield p


class ProjectionUseCaseWithoutTrainingset(ProjectionUseCase):
    def __init__(self, *args):
        super(ProjectionUseCaseWithoutTrainingset, self).__init__()

    def __iter__(self):
        print(
            f"iterator without {self._isStarted} {self._numPoints}", flush=True)
        while self._isStarted:
            p = pb.Point(id=self._numPoints, x=random.randint(
                0, 100), y=random.randint(0, 100))
            self._numPoints = self._numPoints + 1
            yield p


# ProjectionUseCase.register(ProjectionUseCaseWithTrainingset)
# ProjectionUseCase.register(ProjectionUseCaseWithoutTrainingset)
