#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

# with open('README.rst') as readme_file:
#     readme = readme_file.read()

# with open('HISTORY.rst') as history_file:
#     history = history_file.read()

requirements = [
    # TODO: put package requirements here
    'wheel',
    'python-dotenv',
    'dirtyjson'
]

test_requirements = [
    # TODO: put package test requirements here
    'pytest',
    'pytest-cov'
]

setup(
    name='dummyprojector',
    version='0.0.0',
    description="A dapr microservice with grpc frontend",
    long_description="readme",
    author="Michael Behrisch",
    author_email='m.behrisch@uu.nl',
    url='https://git.science.uu.nl/vig/provee/dummy-projector',
    packages=setuptools.find_packages(),
    package_dir={'dummyprojector': 'dummyprojector'},
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='scagnostics, CNN, visual quality metrics, information visualization',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Researchers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
