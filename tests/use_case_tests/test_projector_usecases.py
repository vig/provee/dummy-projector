from dummyprojector.use_cases import projection_use_cases as uc
from dummyprojector.protos_generated import projector_pb2 as pb

import pytest
import numpy as np
from random import randrange


def test_init_without():
    usecaseWithoutTrainingset = uc.ProjectionUseCaseWithoutTrainingset()
    assert usecaseWithoutTrainingset


def test_init_with_Trainingset():
    hdvector = np.random.rand(1, 1000)[0]
    row = pb.TrainingSetRow(id='ab', hdvector=hdvector)

    rows = []
    rows.append(row)

    trainingSet = pb.TrainingSet(rows=rows, modelid="modeltest")
    usecaseWithoutTrainingset = uc.ProjectionUseCaseWithTrainingset(
        trainingSet)
    assert usecaseWithoutTrainingset


def test_retrieve_without_limit():
    proj_use_case = uc.ProjectionUseCaseWithoutTrainingset()

    limitToCheck = randrange(1000)

    for point in proj_use_case.get_iterator():
        assert point
        print("retrieving Point no limit with %s id and x = %s, y = %s" %
              (point.id, point.x, point.y))
        limitToCheck = limitToCheck - 1
        if limitToCheck == 0:
            break

    assert limitToCheck == 0


def test_retrieve_with_trainingset():
    limitToCheck = randrange(1000)

    rows = []
    for i in range(limitToCheck):
        hdvector = np.random.rand(1, 1000)[0]
        row = pb.TrainingSetRow(id='ab' + str(i), hdvector=hdvector)
        rows.append(row)

    trainingSet = pb.TrainingSet(rows=rows, modelid="modeltest")

    proj_use_case = uc.ProjectionUseCaseWithTrainingset(
        trainingSet)

    for point in proj_use_case.get_iterator():
        assert point
        print("retrieving Point with %s id and x = %s, y = %s" %
              (point.id, point.x, point.y))
        limitToCheck = limitToCheck - 1
        if limitToCheck == 0:
            break

    notInside = True
    for point in proj_use_case.get_iterator():
        assert not point
        notInside = False
        print("retrieving a point after iterator is depleated  with %s id and x = %s, y = %s" %
              (point.id, point.x, point.y))

    assert notInside == True
    assert limitToCheck == 0
